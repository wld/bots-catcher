<?php

namespace searchBotsCatcher\bot\api;

use yii\web\ForbiddenHttpException;
use searchBotsCatcher\bot\models\Security;
use searchBotsCatcher\bot\models\repositories\RepositoryDb;

class ApiV1Controller extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    protected $signKey;

    public function init()
    {
        if(!$this->signKey && empty(\Yii::$app->params['searchBotsCatcher-signKey'])){
            throw new ForbiddenHttpException('SignKey not configurated');
        }
        $this->signKey = !$this->signKey ? \Yii::$app->params['searchBotsCatcher-signKey'] : $this->signKey;
        parent::init();
    }

    public function beforeAction($action)
    {
        try {
            if (!$this->checkSign()) throw new ForbiddenHttpException('Wrong Sign');
            return parent::beforeAction($action);
        } catch (ForbiddenHttpException $e) {
            $this->actionError($e->getMessage());
        }
    }

    public function runAction($id, $params = [])
    {
        try {
            parent::runAction($id, $params);
        } catch (\Exception $e) {
            \Yii::error($e->getMessage() . "\n\r" . $e->getTraceAsString());
            $this->actionError($e->getMessage());
        }
    }

    protected function checkSign(){
        $sign = \Yii::$app->request->headers->get(Security::HASH_ID_HEADER_NAME, false);
        if (Security::createSign(\Yii::$app->request->post(), $this->signKey)!==$sign) {
            return false;
        }
        return true;
    }

    public function actionError($message = '')
    {
        return $this->asJson(['status'=>'error', 'message'=>$message]);
    }
}