<?php

use yii\db\Migration;

/**
 * Class m190312_100632_add_unique_index_href_search_bc_links
 */
class m190312_100632_add_unique_index_href_search_bc_links extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx_search_bc_links_href_unique', '{{%search_bc_links}}', [ 'href'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190312_100632_add_unique_index_href_search_bc_links cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190312_100632_add_unique_index_href_search_bc_links cannot be reverted.\n";

        return false;
    }
    */
}
