<?php

use yii\db\Migration;

/**
 * Class m190703_144733_alter_name_search_bc_clients
 */
class m190703_144733_alter_name_search_bc_clients extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%search_bc_clients}}', 'name', $this->string(2048));
        $this->createIndex('search_bc_clients_name', '{{%search_bc_clients}}', 'name', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190703_144733_alter_name_search_bc_clients cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190703_144733_alter_name_search_bc_clients cannot be reverted.\n";

        return false;
    }
    */
}
