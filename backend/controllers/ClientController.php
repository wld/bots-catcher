<?php

namespace searchBotsCatcher\bot\backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * ClientController implements the CRUD actions for SearchBcLinks model.
 */
class ClientController extends \yii\web\Controller
{
    public function getModelName($type = 'index')
    {
        switch ($type) {
            case 'search':
                return 'searchBotsCatcher\bot\models\SearchBcClientsSearch';
            default:
                return 'searchBotsCatcher\bot\models\SearchBcClients';
        }
    }

    public function findModel($id)
    {
        $modelName = $this->getModelName();
        if (($model = $modelName::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actions()
    {
        $this->setViewPath('@searchBotsCatcher/bot/backend/view/client');

        return [
            'create' => [
                'class' => 'searchBotsCatcher\bot\backend\controllers\actions\CreateAction'
            ],
            'index' => [
                'class' => 'searchBotsCatcher\bot\backend\controllers\actions\IndexAction'
            ],
            'update' => [
                'class' => 'searchBotsCatcher\bot\backend\controllers\actions\UpdateAction'
            ],
            'view' => [
                'class' => 'searchBotsCatcher\bot\backend\controllers\actions\ViewAction'
            ],
            'delete' => [
                'class' => 'searchBotsCatcher\bot\backend\controllers\actions\DeleteAction'
            ],
        ];
    }
}