<?php

namespace searchBotsCatcher\bot\api\v1;

use searchBotsCatcher\bot\api\ApiV1Controller;
use searchBotsCatcher\bot\models\methods\ReportAboutBot;
use searchBotsCatcher\bot\models\methods\GetLinks;
use searchBotsCatcher\bot\models\repositories\RepositoryDb;

class CatcherController extends ApiV1Controller
{
    public $enableCsrfValidation = false;

    public function actionGetLinks(){
        $model = new GetLinks();
        $model->setAttributes(\Yii::$app->request->post());
        $repository = new RepositoryDb();
        $links = $repository->getLinks($model);
        return $this->asJson(['data' => $links]);

    }


    public function actionReportAboutBot(){
        $model = new ReportAboutBot();
        $model->setAttributes(\Yii::$app->request->post());
        if($model->validate()){
            $repository = new RepositoryDb();
            if($repository->reportAboutBot($model)){
                return $this->asJson(['data' => ['result' => true]]);
            }
            return $this->asJson(['data' => ['result' => false]]);
        }
        return $this->asJson(['data' => ['result' => false], 'errors' => $model->getErrors()]);
    }

}