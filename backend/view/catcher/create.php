<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model searchBotsCatcher\bot\models\SearchBcLinks */

$this->title = 'Create Search Bc Links';
$this->params['breadcrumbs'][] = ['label' => 'Search Bc Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="search-bc-links-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>