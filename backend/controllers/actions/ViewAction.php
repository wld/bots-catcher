<?php

namespace searchBotsCatcher\bot\backend\controllers\actions;

use Yii;
use yii\base;

class ViewAction extends base\Action
{
    /**
     * Displays a single model.
     * @param integer $id
     * @return mixed
     */
    public function run($id)
    {
        return $this->controller->render('view', [
            'model' => $this->controller->findModel($id),
        ]);
    }
}