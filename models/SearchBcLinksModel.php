<?php

namespace searchBotsCatcher\bot\models;

use Yii;

/**
 * This is the model class for table "search_bc_links".
 *
 * @property integer $id
 * @property string $link
 * @property string $href
 * @property boolean $active
 * @property integer $google_count
 * @property integer $bing_count
 * @property string $created_at
 * @property string $updated_at
 * @property integer $google_count_inactive
 * @property integer $bing_count_inactive
 * @property integer $priority
 * @property integer $client_id
 */
class SearchBcLinksModel extends \yii\base\Model
{
    public $id;
    public $anchor;
    public $href;
    public $active;
    public $google_count;
    public $bing_count;
    public $created_at;
    public $updated_at;
    public $google_count_inactive;
    public $bing_count_inactive;
    public $priority;
    public $client_id;
}
