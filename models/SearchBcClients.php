<?php

namespace searchBotsCatcher\bot\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "search_bc_links".
 *
 * @property integer $id
 * @property string $name
 *
 */
class SearchBcClients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search_bc_clients';
    }

    /**
     * @inheritdoc
     */
    public static function getTableName()
    {
        return self::tableName();
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 2048],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public static function getAllActive(){
        return static::find()->all();
    }

    public static function getClientsForGrid() {
        $clients = self::find()->select('id,name')->asArray()->all();
        foreach ($clients as $client) {
            $clientsForGrid[$client['id']] = $client['name'];
        }
        return $clientsForGrid;
    }
}
