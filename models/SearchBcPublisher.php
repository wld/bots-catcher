<?php

namespace searchBotsCatcher\bot\models;

use Yii;

/**
 * This is the model class for table "search_bc_publisher".
 *
 * @property integer $id
 * @property string $host
 */
class SearchBcPublisher extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'search_bc_publisher';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['host'], 'required'],
            [['host'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => 'Host',
        ];
    }
}
