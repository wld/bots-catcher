<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Import links');
$this->params['breadcrumbs'][] = ['label' => 'Search Bc Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Import';

?>

<h1><?= Html::encode($this->title) ?></h1>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'csvFile')->fileInput()->label('Imported File') ?>

<div style="margin-top: 20px;"></div>
<div class="form-inline">
    <div class="form-group">
        <?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<?php ActiveForm::end() ?>
<br>
<a href="/search-bots-catcher-crm/import-example">Example of CSV</a>
