<?php

namespace searchBotsCatcher\bot\backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use searchBotsCatcher\bot\models\SearchBcLinks;
use searchBotsCatcher\bot\models\SearchBcVisitsSearch;
use searchBotsCatcher\bot\backend\models\CsvFile;
use searchBotsCatcher\bot\models\SearchBcLinksSearch;
use searchBotsCatcher\bot\backend\models\IndexListActions;

/**
 * CatcherController implements the CRUD actions for SearchBcLinks model.
 */
class CatcherController extends \yii\web\Controller
{
    public function getModelName($type = 'index')
    {
        switch ($type) {
            case 'search':
                return 'searchBotsCatcher\bot\models\SearchBcLinksSearch';
            default:
                return 'searchBotsCatcher\bot\models\SearchBcLinks';
        }
    }

    public function findModel($id)
    {
        $modelName = $this->getModelName();
        if (($model = $modelName::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actions()
    {
        $this->setViewPath('@searchBotsCatcher/bot/backend/view/catcher');

        return [
            'delete' => [
                'class' => 'searchBotsCatcher\bot\backend\controllers\actions\DeleteAction'
            ],
        ];
    }

    public function actionIndex(){
        $searchModel = new SearchBcLinksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $actionsModel = new IndexListActions();
        $actionsModel->load(Yii::$app->request->post());
        $actionsModel->setSearchModel($searchModel);
        $actionsModel->setDataProvider($dataProvider);
        $actionsResult = $actionsModel->apply();
        if(is_array($actionsResult) && isset($actionsResult['id_list'])){
            return $this->redirect('/search-bots-catcher-crm?SearchBcLinksSearch[id_list]=' . implode(',', $actionsResult['id_list']),  302);
        }
        if($actionsResult){
            return $this->redirect(\Yii::$app->request->url,  302);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionsModel' => $actionsModel,
        ]);
    }

    public function actionCreate(){
        $model = new SearchBcLinks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            $model->google_count_inactive = 3;
            $model->bing_count_inactive = 3;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            //TODO move functionality to another place ----------- START ---------------
            /*$repositoryRemote = new RepositoryRemote();
            $publisher = SearchBcPublisher::findAll();
            foreach ($publisher as $publishe){
                $repositoryRemote->resetLinks($publishe);
            }*/
            //TODO move functionality to another place ----------- END ---------------

            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionView($id){
        $searchModel = new SearchBcVisitsSearch(['link_id' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionImport()
    {
        set_time_limit(6000);
        $csvFile = new CsvFile();

        if ($csvFile->load(\Yii::$app->getRequest()->post())) {
            $successCnt = 0;
            foreach ($csvFile->rowGenerator(['anchor', 'href']) as $key => $row){
                try {
                    $model = new SearchBcLinks([
                        'anchor'        => $row['anchor'],
                        'href'          => $row['href'],
                        'active'        => true,
                        'google_count'  => 0,
                        'bing_count'    => 0,
                    ]);
                    try{
                        if($model->save()){
                            $successCnt++;
                        }
                    } catch (\Exception $e){}

                } catch(\Exception $ex) {
                    \Yii::warning($ex->getMessage(), __METHOD__);
                    continue;
                }
            }

            \Yii::$app->getSession()->setFlash('success', "{$successCnt} links was imported");
        }

        return $this->render('import', ['model' => $csvFile]);
    }

    public function actionImportExample(){
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=import_search_bc_links.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        return $this->renderFile('@searchBotsCatcher/bot/backend/view/import_search_bc_links.csv');
    }

    public function actionIdListExample(){
        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename=id_list_search_bc.txt");
        header("Pragma: no-cache");
        header("Expires: 0");
        return $this->renderFile('@searchBotsCatcher/bot/backend/view/id_list_search_bc.txt');
    }


}