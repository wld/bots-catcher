<?php
namespace searchBotsCatcher\bot\models\methods;

use yii\base\Model;
use searchBotsCatcher\bot\models\Bot;

class GetLinks extends Model
{
    public $botId;
    public $count = 250;
    public $client;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientId'], 'required'],
            [['botId'], 'in',  'range' => array_keys(Bot::getTitles())],
            [['count'], 'integer'],
            [['client'], 'string'],
        ];
    }
}