<?php

namespace searchBotsCatcher\bot\backend\controllers\actions;

use Yii;
use yii\base;

class DeleteAction extends base\Action
{
    public function run()
    {
        $id = Yii::$app->getRequest()->get('id');
        $modelName = $this->controller->getModelName();
        $model = new $modelName();
        $model->findOne($id)->delete();
        return $this->controller->redirect(['index']);
    }
}