<?php

namespace searchBotsCatcher\bot\models;

class Security
{
    const HASH_ID_HEADER_NAME = 'Request-Sign';

    public static function createSign($array, $signKey){
        ksort($array);
        $queryData = http_build_query($array);

        return hash_hmac('sha256', $queryData, $signKey);
    }
}