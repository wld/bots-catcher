<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use extension\yii\grid\EGridView;
use searchBotsCatcher\bot\models\Bot;

/* @var $this yii\web\View */
/* @var $model searchBotsCatcher\bot\models\SearchBcLinks */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Search Bc Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$bots = Bot::getTitles();

//var_dump($dataProvider->models[0]->userAgent)

?>
<div class="search-bc-links-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'anchor',
            'href',
            'active:boolean',
            'google_count',
            'bing_count',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <?= EGridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'bot_id',
                'format'    => 'raw',
                'label'     => 'Bot',
                'filter'    => false,
                'value'     => function ($data) use ($bots) {
                    return $bots[$data->bot_id];
                }
            ],
            [
                'attribute' => 'userAgent.name',
                'format'    => 'raw',
                'label'     => 'User Agent',
                'filter'    => false,

            ],
            'ip.ip',
            'created_at',
        ],
    ]); ?>

</div>