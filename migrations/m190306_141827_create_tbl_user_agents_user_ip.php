<?php

use yii\db\Migration;

/**
 * Class m190306_141827_create_tbl_user_agents_user_ip
 */
class m190306_141827_create_tbl_user_agents_user_ip extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%search_bc_user_agents}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(512)->notNull(),
        ]);

        $this->createTable('{{%search_bc_user_ips}}', [
            'id' => $this->primaryKey(),
            'ip' => 'inet',
        ]);

        $this->addColumn('{{%search_bc_visits}}', 'user_agent_id', $this->integer());
        $this->addColumn('{{%search_bc_visits}}', 'ip_id', $this->integer());
        $this->addForeignKey('fk_user_agent_id_user_agent_id', '{{search_bc_visits}}', 'user_agent_id', '{{%search_bc_user_agents}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_agent_id_user_ip_id', '{{search_bc_visits}}', 'ip_id', '{{%search_bc_user_ips}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('idx_search_bc_user_agents_name', '{{%search_bc_user_agents}}', 'name', true);
        $this->createIndex('idx_search_bc_user_ips_ip', '{{%search_bc_user_ips}}', 'ip', true);

        $this->createIndex('idx_search_bc_links_href_active', '{{%search_bc_links}}', [ 'href', 'active' ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190306_141827_create_tbl_user_agents_user_ip cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190306_141827_create_tbl_user_agents_user_ip cannot be reverted.\n";

        return false;
    }
    */
}
