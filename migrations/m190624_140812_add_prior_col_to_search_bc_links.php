<?php

use yii\db\Migration;

/**
 * Class m190624_140812_add_prior_col_to_search_bc_links
 */
class m190624_140812_add_prior_col_to_search_bc_links extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%search_bc_links}}', 'priority', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190624_140812_add_prior_col_to_search_bc_links cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190624_140812_add_prior_col_to_search_bc_links cannot be reverted.\n";

        return false;
    }
    */
}
