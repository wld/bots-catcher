<?php
namespace searchBotsCatcher\bot\models\methods;

use yii\base\Model;
use searchBotsCatcher\bot\models\Bot;


class ReportAboutBot extends Model
{
    const COUNT_TO_DEACTIVATE = 3;

    public $href;
    public $botId;
    public $dateTime;
    public $ip;
    public $userAgent;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['botId', 'href', 'dateTime', 'ip', 'userAgent'], 'required'],
            [['botId'], 'in',  'range' => array_keys(Bot::getTitles())],
            //[['href'], 'url'],
            [['href'], 'string'],
            [['userAgent'], 'string'],
            [['ip'], 'ip'],
       //     [['dateTime'], 'date', 'format' => 'Y-m-d H:m:sO']
        ];
    }

    public function getBotFieldName($botId){
        $list = [
            Bot::GOOGLE_BOT => 'google_count',
            Bot::BING_BOT => 'bing_count'
        ];
        return $list[$botId];
    }


}