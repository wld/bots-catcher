<?php

namespace searchBotsCatcher\bot\models;

use Yii;

/**
 * This is the model class for table "search_bc_user_agents".
 *
 * @property integer $id
 * @property string $name
 *
 * @property SearchBcVisits[] $searchBcVisits
 */
class SearchBcUserAgents extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'search_bc_user_agents';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchBcVisits()
    {
        return $this->hasMany(BaseSearchBcVisits::className(), ['user_agent_id' => 'id']);
    }
}
