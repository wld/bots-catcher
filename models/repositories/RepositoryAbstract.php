<?php

namespace searchBotsCatcher\bot\models\repositories;

use searchBotsCatcher\bot\models\methods\GetLinks;
use searchBotsCatcher\bot\models\methods\ReportAboutBot;

abstract class RepositoryAbstract
{
    public $host;
    public $signKey;

    /**
     * @param GetLinks $model
     * @return SearchBcLinks[]
     */
    abstract public function getLinks(GetLinks $model);

    /**
     * @param ReportAboutBot $model
     * @return boolean
     */
    abstract public function reportAboutBot(ReportAboutBot $model);

    /**
     * @param $id
     * @return \searchBotsCatcher\bot\models\SearchBcClients
     */
    abstract public function getClient($id);
}