<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\ActiveField;
use yii\bootstrap\BootstrapPluginAsset;

BootstrapPluginAsset::register($this);
/* @var $this yii\web\View */
/* @var $filtersModel searchBotsCatcher\bot\backend\models\IndexListActions */

?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel  panel-info">
        <div class="panel-heading" role="tab" id="headingOne">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                <div>Actions<span class="caret"></span></div>
            </a>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['layout' => 'horizontal',]);
                $fieldButton = new ActiveField(['form' => $form, 'model'=>$filtersModel]);
                ?>

                <?= $form->field($filtersModel, 'priority', [
                    'template' => $fieldButton->template . '<div class="col-sm-3"><button type="submit" onclick="this.value=1; return true;" name="' . $filtersModel->formName() .'[priority_action]" class="btn btn-info">Apply</button></div>'
                ])->textInput() ?>

                <?= $form->field($filtersModel, 'bot_count_google', [
                    'template' => $fieldButton->template . '<div class="col-sm-3"><button type="submit" onclick="this.value=1; return true;" name="' . $filtersModel->formName() .'[bot_count_google_action]" class="btn btn-info">Apply</button></div>'
                ])->textInput() ?>

                <?= $form->field($filtersModel, 'bot_count_bing', [
                    'template' => $fieldButton->template . '<div class="col-sm-3"><button type="submit" onclick="this.value=1; return true;" name="' . $filtersModel->formName() .'[bot_count_bing_action]" class="btn btn-info">Apply</button></div>'
                ])->textInput() ?>

                <?= $form->field($filtersModel, 'active', [
                    'template' => $fieldButton->template . '<div class="col-sm-3"><button type="submit" onclick="this.value=1; return true;" name="' . $filtersModel->formName() .'[active_action]" class="btn btn-info">Apply</button></div>'
                ])->textInput() ?>

                <?= $form->field($filtersModel, 'id_list_file', [
                    'template' => "{label}\n{beginWrapper}\n{input}\n<span class=\"help-block\" id=\"helpBlock\"><a href='/search-bots-catcher-crm/id-list-example'>Example of file</a></span>\n{error}\n{endWrapper}\n{hint}" .
                        '<div class="col-sm-3"><button type="submit" type="submit" onclick="this.value=1; return true;" name="' . $filtersModel->formName() .'[id_list_file_action]" class="btn btn-info">Apply</button></div>'
                ])->fileInput() ?>

                <div class="form-group">
                    <div class="col-sm-12">
                        <?= Html::submitButton('Remove filtered rows', ['onclick' => 'if(confirm("Remove filtered rows")){this.value=1; return true;}return false;', 'class' => 'btn btn-danger', 'name' => $filtersModel->formName() . '[remove_action]']) ?>
                        <?= Html::submitButton('Reset client in filtered rows', ['onclick' => 'if(confirm("Reset client in filtered rows")){this.value=1; return true;}return false;', 'class' => 'btn btn-danger', 'name' => $filtersModel->formName() . '[reset_client_action]']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
