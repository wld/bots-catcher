<?php
namespace searchBotsCatcher\bot\models;

class Bot
{
    const GOOGLE_BOT = 1;
    const BING_BOT = 2;

    protected static $titles = [
        self::GOOGLE_BOT => 'Googlebot',
        self::BING_BOT => 'Bingbot'
    ];

    protected static $masks = [
        self::GOOGLE_BOT => 'Googlebot/',
        self::BING_BOT =>  'bingbot/'
    ];

    public static function getMasks()
    {
        return self::$masks;
    }

    public static function getTitles()
    {
        return self::$titles;
    }
}