<?php

use yii\db\Migration;

/**
 * Class m190304_164515_create_tbl_search_bots_links
 */
class m190304_164515_create_tbl_search_bots_links extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%search_bc_links}}', [
            'id' => $this->primaryKey(),
            'anchor' => $this->string()->notNull(),
            'href' => $this->string()->notNull(),
            'active' => $this->boolean(),
            'google_count' => $this->integer()->defaultValue(0),
            'bing_count' => $this->integer()->defaultValue(0),
            'created_at'              => 'datetime with time zone default now()',
            'updated_at'              => 'datetime with time zone default now()',
        ]);

        $this->createTable('{{%search_bc_visits}}', [
            'id' => $this->primaryKey(),
            'link_id' => $this->integer()->notNull(),
            'bot_id' => $this->string()->notNull(),
            'created_at'              => 'datetime with time zone default now()',
        ]);
        $this->addForeignKey('fk_search_bc_visits_link_id', '{{search_bc_visits}}', 'link_id', '{{%search_bc_links}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190304_164515_create_tbl_search_bots_links cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190304_164515_create_tbl_search_bots_links cannot be reverted.\n";

        return false;
    }
    */
}
