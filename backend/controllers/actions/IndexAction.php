<?php

namespace searchBotsCatcher\bot\backend\controllers\actions;

use Yii;
use yii\base;

class IndexAction extends base\Action
{
    /**
     * Lists all models.
     * @return mixed
     */
    public function run()
    {
        $modelName = $this->controller->getModelName('search');
        $searchModel = new $modelName();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->controller->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}