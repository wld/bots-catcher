<?php

namespace searchBotsCatcher\bot\models;

use Yii;

/**
 * This is the model class for table "search_bc_visits".
 *
 * @property integer $id
 * @property integer $link_id
 * @property string $bot_id
 * @property string $created_at
 * @property string $ip_id
 * @property string $user_agent_id
 *
 * @property SearchBcLinks $link
 * @property SearchBcLinks[] $searchBcVisits
 */
class SearchBcVisits extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'search_bc_visits';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link_id', 'bot_id'], 'required'],
            [['link_id', 'ip_id', 'user_agent_id'], 'integer'],
            [['created_at'], 'safe'],
            [['bot_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link_id' => 'Link ID',
            'bot_id' => 'Bot ID',
            'created_at' => 'Created At',
            'ip_id' => 'IP id',
            'user_agent_id' => 'User Agent Id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIp()
    {
        return $this->hasOne(SearchBcUserIps::className(), ['id' => 'ip_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAgent()
    {
        return $this->hasOne(SearchBcUserAgents::className(), ['id' => 'user_agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(SearchBcLinks::className(), ['id' => 'link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchBcVisits()
    {
        return $this->hasMany(SearchBcLinks::className(), ['link_id' => 'id']);
    }
}
