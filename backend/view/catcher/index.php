<?php

use kartik\widgets\Select2;
use searchBotsCatcher\bot\models\SearchBcClients;
use yii\helpers\Html;
use extension\yii\grid\EGridView;
use kartik\field\FieldRange;
use dosamigos\datepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel searchBotsCatcher\bot\models\SearchBcLinksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $actionsModel searchBotsCatcher\bot\backend\models\IndexListActions */

$this->title = 'Search Bc Links';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="search-bc-links-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Search Bc Links', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Import', ['import'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="container">
        <div class="row">
            <div class="col-md-6"><?= $this->render('./index_actions', ['filtersModel' => $actionsModel]); ?></div>
            <div class="col-md-6"></div>
        </div>

        <div class="row">
            <div class="col-md-12">
            <?= EGridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'value' => function ($data) {
                            return $data['id'];
                        },
                        'filter' => FieldRange::widget([
                            'model' => $searchModel,
                            'attribute1' => 'id_from',
                            'value1' => '',
                            'attribute2' => 'id_to',
                            'value2' => '',
                            'separator' => '-',
                            'useAddons' => false,
                            'template' => '{widget}{error}',
                            'widgetContainer' => [
                                'style' => 'margin-bottom: -15px; width: 200px',
                            ],
                            'type' => FieldRange::INPUT_TEXT,
                        ]),
                        'contentOptions' => ['style' => 'width:200px'],
                    ],
                    'href',
                    [
                        'attribute' => 'client_id',
                        'value' => function ($data) {
                            $client = $data->getSearchBcClient()->one();
                            return $client['name'];
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'client_id',
                            'data' => SearchBcClients::getClientsForGrid(),
                            'options' => ['placeholder' => 'Select a state ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                        'contentOptions' => ['style' => 'width:200px'],
                    ],
                    'active:boolean',
                    'priority',
                    [
                        'label' => 'Google',
                        'attribute' => 'google_count'
                    ],
                    [
                        'label' => 'Bing',
                        'attribute' => 'bing_count'
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function ($data) {
                            return $data['created_at'];
                        },
                        'filter' => DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'created_at_from',
                            'attributeTo' => 'created_at_to',
                            'clientOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'clearBtn' => true,
                            ],
                            'options' => ['style' => 'width:100px !important'],
                            'optionsTo' => ['style' => 'width:100px !important'],
                        ]),
                        //'options' => ['style' => 'width:200px !important'],
                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
        </div>

    </div>


</div>