<?php

namespace searchBotsCatcher\bot\models;

use Yii;

/**
 * This is the model class for table "search_bc_user_ips".
 *
 * @property integer $id
 * @property string $ip
 *
 * @property SearchBcVisits[] $searchBcVisits
 */
class SearchBcUserIps extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'search_bc_user_ips';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchBcVisits()
    {
        return $this->hasMany(BaseSearchBcVisits::className(), ['ip_id' => 'id']);
    }
}
