<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model searchBotsCatcher\bot\models\SearchBcLinks */

$this->title = 'Update Search Bc Links: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Search Bc Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="search-bc-links-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>