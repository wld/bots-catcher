<?php

use yii\db\Migration;

/**
 * Class m190307_143946_create_search_bc_publisher
 */
class m190307_143946_create_search_bc_publisher extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%search_bc_publisher}}', [
            'id' => $this->primaryKey(),
            'host' => $this->string()->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190307_143946_create_search_bc_publisher cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190307_143946_create_search_bc_publisher cannot be reverted.\n";

        return false;
    }
    */
}
