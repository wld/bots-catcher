# Установка в качестве ЦРМ и API
- Выполнить миграции
`php yii migrate --migrationPath=@searchBotsCatcher/bot/migrations --interactive=0`
- Зарегистрировать в параметрах ключ АПИ. В файле .. common/config/params-local.php добавляем параментр
 ``` 
'searchBotsCatcher-signKey' => '123123' 
``` 
- Зарегистрировать в bootstrap каласс для обработки запросов к АПИ. В файле ../config/main-local.php добавляем
``` 
$config = [ 'bootstrap' => [..., 'searchBotsCatcher\bot\BootstrapApi'],]; 
```

**После регистрации станут доступны методы АПИ(на домене, на котором была зарегимтрированна АПИ)**
* /search-bots-catcher/get-links
* /search-bots-catcher/report-about-bot

**В CRM появится раздел**
* /search-bots-catcher-crm 


# Установка в качестве публикатора ссылок
- Зарегистрировать в компоненту ловца ботов. В файле ../config/main-local.php
 ``` 
'botsCatcher' => [
            'class' => 'searchBotsCatcher\bot\components\CatcherComponent',
            'repository' => 'searchBotsCatcher\bot\models\repositories\RepositoryRemote',
            'host' => 'http://api.es-crm.local', //хост на котором размещается АПИ
            'signKey' => '123123' //ключ АПИ
	    'cache' => 'cache', //компоненте кэша - если нету, не обязательно
            'cacheDuration' => 3600 //, не обязательно
        ],
``` 
- Пример получения ссылок с компоненты
``` 
/* @var $botsCatcher \searchBotsCatcher\bot\components\CatcherComponent */
/* @var $links \searchBotsCatcher\bot\models\SearchBcLinksModel[] */
$botsCatcher = \Yii::$app->botsCatcher;
$links = $botsCatcher->getLinks(client,count);
где
count - кол-во ссылок которое надо получить
client - URL к которому привязывается список ссылок, если запросить второй раз с тем же clientId - отдастся тот-же список ссылок
``` 

# Установка в качестве ловца ботов
- Зарегистрировать в компоненту ловца ботов. В файле ../config/main-local.php
``` 
'botsCatcher' => [
            'class' => 'searchBotsCatcher\bot\components\CatcherComponent',
            'repository' => 'searchBotsCatcher\bot\models\repositories\RepositoryRemote',
            'host' => 'http://api.es-crm.local', //хост на котором размещается АПИ
            'signKey' => '123123' //ключ АПИ
        ],
``` 
- Пример сообщениея о боте. Можно использовать, например в методе init() контроллера
``` 
/* @var $botsCatcher \searchBotsCatcher\bot\components\CatcherComponent */
$botsCatcher = \Yii::$app->botsCatcher;
$botsCatcher->analyzeRequest(\Yii::$app->request);
``` 


# Описание методов АПИ

 # Массыв ссылок, для показа
`- /search-bots-catcher/get-links`

Параметры: 
* `count` - кол-во ссылок которое надо получить
* `client` - URL к которому привязывается список ссылок, если запросить второй раз с тем же clientId - отдастся тот-же список ссылок

**Пример ответа**
```
{
    "data": [
        {
            "id": 2,
            "anchor": "The strangers",
            "href": "http://es-student-org/visual-arts-film-studies/1684101-the-strangers",
            "active": true,
            "google_count": 1,
            "bing_count": 0,
            "created_at": "2019-03-11 11:32:13+02",
            "updated_at": "2019-03-11 11:03:17+02"
        },
        {
            "id": 1,
            "anchor": "Student-help5",
            "href": "http://es-student-org/student-help",
            "active": true,
            "google_count": 4,
            "bing_count": 0,
            "created_at": null,
            "updated_at": "2019-03-11 12:03:09+02"
        }
    ]
}
```

   
 # Сообщить о том что ссылку посетил поисковый бот
`- /search-bots-catcher/report-about-bot`

Параметры: 
* `href` - ссылка с /search-bots-catcher/get-links
* `botId` - id бота [Googlebot=1, Bingbot=2]
* `dateTime` - дата и время посещения бота в формате Y-m-d H:m:sO пример "2019-03-11 11:03:17+02"
* `ip` - id бота
* `userAgent` - userAgent бота

**Ответ:** true/false

