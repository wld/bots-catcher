<?php

namespace searchBotsCatcher\bot\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SearchBcLinksSearch represents the model behind the search form about `searchBotsCatcher\bot\models\SearchBcLinks`.
 */
class SearchBcVisitsSearch extends SearchBcVisits
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'link_id', 'bot_id', 'user_agent_id'], 'integer'],
            [['ip_id'], 'ip'],
            [['bot_id', 'link_id', 'created_at', 'ip_id', 'user_agent_id'], 'safe'],
        ];
    }

    /**
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SearchBcVisits::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'link_id' => $this->link_id,
            'bot_id' => $this->bot_id,
            'user_agent_id' => $this->user_agent_id,
            'ip_id' => $this->ip_id,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}