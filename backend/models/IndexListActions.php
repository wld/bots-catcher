<?php

namespace searchBotsCatcher\bot\backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use searchBotsCatcher\bot\models\SearchBcLinksSearch;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

class IndexListActions extends Model
{
    public $priority;
    public $priority_action;

    public $bot_count_google;
    public $bot_count_google_action;

    public $bot_count_bing;
    public $bot_count_bing_action;

    public $id_list_file;
    public $id_list_file_action;

    public $remove_action;
    public $reset_client_action;

    public $active;
    public $active_action;

    /**
     * @var SearchBcLinksSearch
     */
    protected $searchModel;
    /**
     * @var ActiveDataProvider
     */
    protected $dataProvider;

    public function rules()
    {
        return [
            [['priority', 'bot_count_google', 'bot_count_bing','active'], 'integer'],
            [['priority_action', 'bot_count_google_action', 'bot_count_bing_action', 'remove_action', 'reset_client_action', 'active_action'], 'boolean'],
            [['id_list_file'], 'file', 'skipOnEmpty' => true, 'checkExtensionByMimeType' => false, 'extensions' => 'csv, txt'],
        ];
    }

    public function setSearchModel(SearchBcLinksSearch $searchModel){
        $this->searchModel = $searchModel;
    }

    public function setDataProvider(ActiveDataProvider $dataProvider){
        $this->dataProvider = $dataProvider;
    }

    public function apply(){
     //   echo '<pre>'.print_r($this, true) .'</pre>';
        if(($file = UploadedFile::getInstance($this, 'id_list_file'))){
            return $this->idListFileAction($file);
        }
        if(!empty($this->remove_action)){
            return $this->removeAction();
        }
        if(!empty($this->bot_count_google_action)){

            return $this->changeModelsField('google_count_inactive', $this->bot_count_google);
        }
        if(!empty($this->bot_count_bing_action)){
            return $this->changeModelsField('bing_count_inactive', $this->bot_count_bing);
        }
        if(!empty($this->priority_action)){
            return $this->changeModelsField('priority', $this->priority);
        }
        if(!empty($this->reset_client_action)){
            return $this->changeModelsField('client_id', null);
        }
        if(!empty($this->active_action)){
            $active = !empty($this->active) ? 1 : 0;
            return $this->changeModelsField('active', $active);
        }

        return false;
    }

    /**
     * @return array|bool
     */
    protected function idListFileAction($file){
        $ids = file($file->tempName);
        if(!empty($ids)){
            array_walk($ids, function (&$item){
                $item = (int)trim($item);
            });
            $ids = array_filter($ids);
            return ['id_list' => $ids];
        }
        return false;
    }

    /**
     * @return SearchBcLinksSearch[]
     */
    protected function getModels(){
        $this->dataProvider->setPagination(false);
        return $this->dataProvider->getModels();
    }

    protected function removeAction(){
        $models = $this->getModels();
        foreach($models as $model){
            $model->delete();
        }
        return true;
    }

    protected function changeModelsField($fieldName, $fieldValue){
        $models = $this->dataProvider->query->createCommand()->queryAll();
        $modelsIds = ArrayHelper::getColumn($models, 'id');
        unset($models);
        $modelsParts = array_chunk($modelsIds, 300);
        foreach($modelsParts as $modelsPart){
            (new Query())->createCommand()->update(
                $this->searchModel->getTableName(),
                [$fieldName => $fieldValue],
                ['id' => $modelsPart]
            )->execute();
        }
        return true;
    }

}