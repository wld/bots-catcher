<?php

namespace searchBotsCatcher\bot\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "search_bc_links".
 *
 * @property integer $id
 * @property string $anchor
 * @property string $href
 * @property boolean $active
 * @property integer $google_count
 * @property integer $bing_count
 * @property integer $google_count_inactive
 * @property integer $bing_count_inactive
 * @property string $created_at
 * @property string $updated_at
 * @property integer $priority
 * @property integer $client_id
 *
 */
class SearchBcLinks extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'search_bc_links';
    }

    /**
     * @inheritdoc
     */
    public static function getTableName()
    {
        return self::tableName();
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['anchor', 'href'], 'required'],
            [['active'], 'boolean'],
            [['google_count', 'bing_count', 'priority', 'client_id'], 'integer'],
            [['google_count', 'bing_count'], 'default', 'value' => 0],
            [['google_count_inactive', 'bing_count_inactive'], 'default', 'value' => 3],
            [['created_at', 'updated_at'], 'safe'],
            [['anchor', 'href'], 'string', 'max' => 255],
            ['href', 'unique'],
            ['href', 'filter', 'filter' => 'trim'],

        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'anchor' => 'Anchor',
            'href' => 'Href',
            'active' => 'Active',
            'google_count' => 'Google Count',
            'bing_count' => 'Bing Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'google_count_inactive' => 'Google count to inactive',
            'bing_count_inactive' => 'Bing count to inactive',
            'client_id' => 'Client',
        ];
    }

    public static function getBotFieldName($botId){
        $list = [
            Bot::GOOGLE_BOT => 'google_count',
            Bot::BING_BOT => 'bing_count'
        ];
        return $list[$botId];
    }

    public function getSearchBcClient()
    {
        return $this->hasOne(SearchBcClients::className(), ['id' => 'client_id']);
    }
}
