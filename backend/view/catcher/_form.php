<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use searchBotsCatcher\bot\models\SearchBcClients;

/* @var $this yii\web\View */
/* @var $model searchBotsCatcher\bot\models\SearchBcLinks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="search-bc-links-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'anchor')->textInput(['maxlength' => true]) ?>

    <?php if($this->context->action->id == 'create'): ?>
        <?= $form->field($model, 'href')->textInput(['maxlength' => true]) ?>
    <?php else : ?>
        <?= $form->field($model, 'href')->textInput(['maxlength' => true, 'disabled' => true]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <?= $form->field($model, 'google_count_inactive')->textInput(['maxlength' => true])->label('Google count to inactive - <i>if 0 will be disregarded</i>')?>

    <?= $form->field($model, 'bing_count_inactive')->textInput(['maxlength' => true])->label('Bing count to inactive - <i>if 0 will be disregarded</i>') ?>

    <?= $form->field($model, 'priority')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'client_id')->dropDownList(\yii\helpers\ArrayHelper::map(SearchBcClients::getAllActive(), 'id', 'name'), ['prompt'=>'']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>