<?php
namespace searchBotsCatcher\bot\models\repositories;

use searchBotsCatcher\bot\models\methods\GetLinks;
use searchBotsCatcher\bot\models\methods\ReportAboutBot;
use searchBotsCatcher\bot\models\Security;
use searchBotsCatcher\bot\models\SearchBcLinksModel;
use searchBotsCatcher\bot\models\SearchBcPublisher;

class RepositoryRemote extends RepositoryAbstract
{
    /**
     * @param GetLinks $model
     * @return SearchBcLinks[]
     */
    public function getLinks(GetLinks $model){
        $links = [];
        $result = $this->send('/search-bots-catcher/get-links', $model->toArray());
        if($result && !empty($result['data'])){
            foreach ($result['data'] as $row){
                $links[] = new SearchBcLinksModel($row);
            }
        }
        return $links;
    }

    public function reportAboutBot(ReportAboutBot $model){
        if($model->validate()){
            $this->send('/search-bots-catcher/report-about-bot', $model->toArray());
            return true;
        }
        return false;
    }

    public function resetLinks(SearchBcPublisher $model){
        $this->send(trim($model->host, '/') . '/search-bots-catcher-publisher/reset-links', $model->toArray());
    }

    protected function send($path, $data)
    {
        $ch = curl_init($this->host . $path);
        $headers = [Security::HASH_ID_HEADER_NAME .":". Security::createSign($data, $this->signKey)];

        /*echo $this->host . $path;
        print_r($data);
        print_r($headers);
        var_dump( http_build_query($data));
        die();*/

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        try{
            $result = curl_exec($ch);
       //     var_dump($result);
            curl_close($ch);
            return json_decode($result, true);
        } catch (\Exception $e){
            return null;
        }
    }

    protected function sendAsync($path, $data)
    {
        $url = $this->host . $path;
        $post_string = http_build_query($data);
        $host=parse_url($url, PHP_URL_HOST);
        $fp = fsockopen($host, 80, $errno, $errstr, 30);

        if(!$fp){// "Couldn’t open a socket to ".$host." (".$errstr.") " .$errno;
            return false;
        }

        $out = "POST ".$path." HTTP/1.1\r\n";
        $out.= "Host: ".$host."\r\n";
        $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out.= "Content-Length: ".strlen($post_string)."\r\n";
        $out.= Security::HASH_ID_HEADER_NAME .": ".Security::createSign($data, $this->signKey)."\r\n";
        $out.= "Connection: Close\r\n\r\n";
        if (isset($post_string)) $out.= $post_string;

        fwrite($fp, $out);
        fclose($fp);
    }


    /**
     * @param $id
     * @return \searchBotsCatcher\bot\models\SearchBcClients
     */
    public function getClient($id){
        return null;
    }
}