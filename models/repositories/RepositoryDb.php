<?php
namespace searchBotsCatcher\bot\models\repositories;

use searchBotsCatcher\bot\models\methods\GetLinks;
use searchBotsCatcher\bot\models\methods\ReportAboutBot;
use searchBotsCatcher\bot\models\SearchBcVisits;
use searchBotsCatcher\bot\models\SearchBcLinks;
use searchBotsCatcher\bot\models\SearchBcUserIps;
use searchBotsCatcher\bot\models\SearchBcUserAgents;
use searchBotsCatcher\bot\models\Bot;
use searchBotsCatcher\bot\models\SearchBcClients;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class RepositoryDb extends RepositoryAbstract
{
    /**
     * @param GetLinks $model
     * @return SearchBcLinks[]
     */
    public function getLinks(GetLinks $model){
        $clientName = mb_substr($model->client, 0, 2048);
        $client = SearchBcClients::find()->where(['name' => $clientName])->one();
        if(!$client){
            $client =new SearchBcClients();
            $client->setAttributes(['name' => $clientName], false);
            $client->save();
        }
        $links = SearchBcLinks::find()->where(['active' => true])
            ->andWhere(['client_id' => $client->id])
            ->orderBy('"priority" DESC NULLS LAST, created_at DESC')->limit($model->count)->all();

        $countClientLinks = count($links);
        if($countClientLinks < $model->count){
            $count = $model->count - $countClientLinks;
            $noClientLinks = SearchBcLinks::find()->where(['active' => true])
                ->andWhere('client_id IS NULL')
                ->orderBy('"priority" DESC NULLS LAST, created_at DESC')->limit($count)->all();

            if(!empty($noClientLinks)){
                SearchBcLinks::updateAll(['client_id' => $client->id], ['id' => ArrayHelper::getColumn($noClientLinks, 'id')]);
                $links = array_merge($links, $noClientLinks);
            }
        }
        return $links;
    }

    /**
     * @param ReportAboutBot $model
     * @return bool
     */
    public function reportAboutBot(ReportAboutBot $model){
        if($model->validate()){
            $link = SearchBcLinks::find()->where(['href' => $model->href])->limit(1)->one();
            if($link){
                $counterField = SearchBcLinks::getBotFieldName($model->botId);
                $updateColumns = [$counterField => new Expression( $counterField.'+1')];

                /***************************** active/inactive START*****************************/
                $newGoogleCount = ($model->botId == Bot::GOOGLE_BOT) ? ($link->google_count+1) : $link->google_count;
                $newBingCount   = ($model->botId == Bot::BING_BOT) ? ($link->bing_count+1) : $link->bing_count;
                $inactive = null;
                if($link->google_count_inactive){
                    $inactive = $newGoogleCount >= $link->google_count_inactive;
                }
                if($link->bing_count_inactive) {
                    $inactiveBing = $newBingCount >= $link->bing_count_inactive;
                    if(!is_null($inactive)){
                        $inactive &= $inactiveBing;
                    } else {
                        $inactive = $inactiveBing;
                    }
                }
                if(!is_null($inactive)){
                    $updateColumns['active'] = !$inactive;
                }
                /***************************** active/inactive END*****************************/

                \Yii::$app->db->createCommand()->update(
                    SearchBcLinks::tableName(),
                    $updateColumns,
                    ['id' => $link->id]
                )->execute();
                (new SearchBcVisits([
                    'link_id' => $link->id,
                    'bot_id' => $model->botId,
                    'created_at' => $model->dateTime,
                    'ip_id' => $this->getIpId($model),
                    'user_agent_id' => $this->getUserAgentId($model),
                ]))->save();

                return true;
            }
        }
        return false;
    }

    protected function getIpId(ReportAboutBot $model){
        $search = SearchBcUserIps::find()->where(['ip' => $model->ip])->limit(1)->one();
        if(!$search){
            $search = new SearchBcUserIps(['ip' => $model->ip]);
            $search->save();
        }
        return $search->id;
    }

    protected function getUserAgentId(ReportAboutBot $model){
        $search = SearchBcUserAgents::find()->where(['name' => $model->userAgent])->limit(1)->one();
        if(!$search){
            $search = new SearchBcUserAgents(['name' => $model->userAgent]);
            $search->save();
        }
        return $search->id;
    }

    /**
     * @param $id
     * @return \searchBotsCatcher\bot\models\SearchBcClients
     */
    public function getClient($id){
        return SearchBcClients::findOne($id);
    }
}