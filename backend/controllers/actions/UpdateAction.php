<?php

namespace searchBotsCatcher\bot\backend\controllers\actions;

use Yii;
use yii\base;

class UpdateAction extends base\Action
{
    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function run($id)
    {
        /**
         * @var $model \common\models\base\BaseModel
         */

        $model = $this->controller->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->controller->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->controller->render('update', [
                'model' => $model,
            ]);
        }
    }
}