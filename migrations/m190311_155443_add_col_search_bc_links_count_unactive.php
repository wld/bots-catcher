<?php

use yii\db\Migration;

/**
 * Class m190311_155443_add_col_search_bc_links_count_unactive
 */
class m190311_155443_add_col_search_bc_links_count_unactive extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%search_bc_links}}', 'google_count_inactive', $this->integer());
        $this->addColumn('{{%search_bc_links}}', 'bing_count_inactive', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190311_155443_add_col_search_bc_links_count_unactive cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190311_155443_add_col_search_bc_links_count_unactive cannot be reverted.\n";

        return false;
    }
    */
}
