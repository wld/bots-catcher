<?php

namespace searchBotsCatcher\bot\backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class CsvFile extends Model
{
    public $csvFile;

    public function rules()
    {
        return [
            [['csvFile'], 'required'],
            [['csvFile'], 'file', 'skipOnEmpty' => false, 'checkExtensionByMimeType' => false, 'extensions' => 'csv, txt'],
        ];
    }

    /**
     * @param null $fieldsMap
     * @param string $delimiter
     * @return \Generator
     */
    public function rowGenerator($fieldsMap = null, $delimiter = ',')
    {
        $file = UploadedFile::getInstance($this, 'csvFile');
        if ($file && $handle = fopen($file->tempName, "r")) {
            return $this->fromFileGenerator($handle, $fieldsMap, $delimiter);
        }

    }

    public function fromFileGenerator( $handle, $fieldsMap, $delimiter){
        while (($row = fgetcsv($handle, 0, $delimiter)) !== false) {
            if (empty($fieldsMap)) {
                yield $row;
            } else {
                $assocRow = [];
                foreach ($fieldsMap as $number => $field) {
                    if (isset($row[$number])) {
                        $assocRow[$field] = $row[$number];
                    }
                }
                yield $assocRow;
            }
        }
    }
}