<?php

namespace searchBotsCatcher\bot;

class BootstrapPublisher implements \yii\base\BootstrapInterface
{
    public function bootstrap($app){
        $app->controllerMap = array_merge($app->controllerMap, [
            'SearchBotsCatcherPublisher'     => 'searchBotsCatcher\bot\api\v1\PublisherController',
        ]);

        $app->getUrlManager()->addRules([
            'ssearch-bots-catcher-publisher/reset-links' => 'SearchBotsCatcherPublisher/reset-links',
        ], false);
    }
}