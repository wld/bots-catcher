<?php

use yii\helpers\Html;
use extension\yii\grid\EGridView;
use kartik\field\FieldRange;
use dosamigos\datepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel searchBotsCatcher\bot\models\SearchBcClients */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Search Bot Clients';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="search-bc-links-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Client', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Search Bot Links', '/search-bots-catcher-crm', ['class' => 'btn btn-success']) ?>
    </p>
    <div class="container">

        <div class="row">
            <div class="col-md-12">
            <?= EGridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    'name',
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            </div>
        </div>

    </div>


</div>