<?php

namespace searchBotsCatcher\bot\backend\controllers\actions;

use Yii;
use yii\base;

class CreateAction extends base\Action
{
    public $scenario = 'default';
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function run()
    {
        /**
         * @var $model \common\models\base\BaseModel
         */
        $modelName = $this->controller->getModelName();

        $model = new $modelName();
        $model->scenario = $this->scenario;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->controller->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->controller->render('create', [
                'model' => $model,
            ]);
        }

    }
}