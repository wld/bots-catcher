<?php

namespace searchBotsCatcher\bot\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use searchBotsCatcher\bot\models\SearchBcLinks;

/**
 * SearchBcClientsSearch represents the model behind the search form about `searchBotsCatcher\bot\models\SearchBcLinks`.
 */
class SearchBcClientsSearch extends SearchBcClients
{
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SearchBcClients::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
        ]);

        if(empty($params['sort'])) $query->orderBy('id desc');

        return $dataProvider;
    }
}