<?php

namespace searchBotsCatcher\bot\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use searchBotsCatcher\bot\models\SearchBcLinks;

/**
 * SearchBcLinksSearch represents the model behind the search form about `searchBotsCatcher\bot\models\SearchBcLinks`.
 */
class SearchBcLinksSearch extends SearchBcLinks
{
    public $id_from;
    public $id_to;
    public $created_at_from;
    public $created_at_to;
    public $id_list;
    public $client_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'google_count', 'bing_count', 'id_from', 'id_to', 'priority'], 'integer'],
            [['anchor', 'href', 'created_at', 'updated_at', 'created_at_from', 'created_at_to', 'client_id'], 'safe'],
            [['active'], 'boolean'],
            [['id_list'], 'validateIdList'],
        ];
    }

    /**
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Validating customer
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validateIdList($attribute, $params)
    {
        if (!empty($this->$attribute)) {
            $ids = explode(',', $this->$attribute);
            if(!empty($ids)){
                array_walk($ids, function (&$item){
                    $item = (int)trim($item);
                });
                $ids = array_filter($ids);
            }
            if (empty($ids)) {
                $this->addError('customer_id', 'Wrong id_list');
                return false;
            }
            $this->$attribute = $ids;
        }
        return true;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SearchBcLinks::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
            'google_count' => $this->google_count,
            'bing_count' => $this->bing_count,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if ($this->priority === '0') {
            $query->andWhere('priority is null');
        } elseif($this->priority) {
            $query->andWhere(['priority' => $this->priority]);
        }

        if (!empty($this->id_from)) {
            $query->andWhere(['>=', 'id', $this->id_from]);
        }
        if (!empty($this->id_to)) {
            $query->andWhere(['<=', 'id', intval($this->id_to)]);
        }
        if (!empty($this->created_at_from)) {
            $query->andFilterWhere(['>=', 'created_at', $this->created_at_from . ' 00:00:00']);
        }
        if (!empty($this->created_at_to)) {
            $query->andFilterWhere(['<=', 'created_at', $this->created_at_to . ' 23:59:59']);
        }

        if (!empty($this->client_id)) {
            $query->andFilterWhere(['=', 'client_id', $this->client_id]);
        }

        $query->andFilterWhere(['like', 'anchor', $this->anchor])
            ->andFilterWhere(['like', 'href', $this->href]);

        if (!empty($this->id_list)) {
            $query = SearchBcLinks::find()->where(['id' => $this->id_list]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);
        }
      //  echo $query->createCommand()->rawSql;

        if(empty($params['sort'])) $query->orderBy('id desc');

        return $dataProvider;
    }
}