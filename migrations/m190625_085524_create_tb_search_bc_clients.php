<?php

use yii\db\Migration;

/**
 * Class m190625_085524_create_tb_search_bc_clients
 */
class m190625_085524_create_tb_search_bc_clients extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%search_bc_clients}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->addColumn('{{%search_bc_links}}', 'client_id', $this->integer());
        $this->addForeignKey('search_bc_links_client_id', '{{search_bc_links}}', 'client_id', '{{%search_bc_clients}}', 'id', 'CASCADE', 'CASCADE');

        $this->execute("CREATE INDEX idx_search_bc_links_priority_created_at ON search_bc_links USING btree (priority DESC NULLS LAST, created_at DESC NULLS LAST);");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190625_085524_create_tb_search_bc_clients cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190625_085524_create_tb_search_bc_clients cannot be reverted.\n";

        return false;
    }
    */
}
