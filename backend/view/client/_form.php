<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use searchBotsCatcher\bot\models\SearchBcClients;

/* @var $this yii\web\View */
/* @var $model searchBotsCatcher\bot\models\SearchBcClients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="search-bc-links-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>