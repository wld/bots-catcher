<?php
namespace searchBotsCatcher\bot;

class BootstrapApi implements \yii\base\BootstrapInterface
{
    public function bootstrap($app){
        $app->controllerMap = array_merge($app->controllerMap, [
            'SearchBotsCatcher'     => 'searchBotsCatcher\bot\api\v1\CatcherController',
            'search-bots-catcher-crm'  => 'searchBotsCatcher\bot\backend\controllers\CatcherController',
            'search-bots-client-crm'  => 'searchBotsCatcher\bot\backend\controllers\ClientController',
        ]);

        $app->getUrlManager()->addRules([
            'search-bots-catcher/<action:[\w\-]+>' => 'SearchBotsCatcher/<action>',
            'search-bots-catcher-crm' => 'search-bots-catcher-crm/index',
            'search-bots-catcher-crm/<action:[\w\-]+>' => 'search-bots-catcher-crm/<action>',
            'search-bots-client-crm' => 'search-bots-client-crm/index',
            'search-bots-client-crm/<action:[\w\-]+>' => 'search-bots-client-crm/<action>',
        ], false);
    }
}